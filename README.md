# Milligram for Drupal

Milligram is an extra lightweight CSS front-end framework for developing fast and powerful web interfaces with Drupal 8.x

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisities

Milligram runs on Drupal 8.x
Milligram should work with any contrib module.

```
Milligram is for developers and designers. Build rapid prototypes with ease.
```

### Installing

Install Drupal 8

Download the newest version of Milligram base theme, make default theme in /admin/appearrance

```
Do not edit the Milligram theme, make your changes in either one of the contributed sub themes or make your own sub theme.
```

Download desired sub theme. For minimal installations or developers looking to start with a clean slate, download the microgram
